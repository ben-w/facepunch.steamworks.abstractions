﻿using System;
using System.Threading.Tasks;
using Steamworks.Ugc;

namespace Facepunch.Steamworks.Abstractions.Wrappers;

public class ItemWrapper : IItem
{
    public IPublishedFieldId Id { get; }
    public string Title { get; }
    public string Description { get; }
    public bool IsSubscribed { get; }
    public string[] Tags { get; }
    public string PreviewImageUrl { get; }
    public uint VotesDown { get; }
    public uint VotesUp { get; }
    public ulong NumSubscriptions { get; }
    public string OwnerName { get; }
    public DateTime Updated { get; }
    public string MetaData { get; }
    public bool IsPublic { get; }
    public bool IsFriendsOnly { get; }
    public bool IsPrivate { get; }
    public bool IsUnlisted { get; }

    private Item _item;

    public ItemWrapper(Item item)
    {
        Id = new PublishedFieldIdWrapper(item.Id);
        Title = item.Title;
        Description = item.Description;
        IsSubscribed = item.IsSubscribed;
        Tags = item.Tags;
        PreviewImageUrl = item.PreviewImageUrl;
        VotesDown = item.VotesDown;
        VotesUp = item.VotesUp;
        Updated = item.Updated;
        NumSubscriptions = item.NumSubscriptions;
        OwnerName = item.Owner.Name;
        MetaData = item.Metadata;
        IsPublic = item.IsPublic;
        IsFriendsOnly = item.IsFriendsOnly;
        IsPrivate = item.IsPrivate;
        IsUnlisted = item.IsUnlisted;
        _item = item;
    }

    public async Task<bool> Subscribe()
    {
        return await _item.Subscribe();
    }

    public async Task<bool> Unsubscribe()
    {
        return await _item.Unsubscribe();
    }
}