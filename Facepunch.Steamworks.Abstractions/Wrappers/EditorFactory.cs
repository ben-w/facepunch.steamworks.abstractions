﻿namespace Facepunch.Steamworks.Abstractions.Wrappers;

public class EditorFactory : IEditorFactory
{
    public IEditor New(WorkshopFileType type)
    {
        return new EditorWrapper(type);
    }

    public IEditor New(IPublishedFieldId publishedFieldId)
    {
        return new EditorWrapper(publishedFieldId);
    }
}