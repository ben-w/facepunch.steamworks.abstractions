﻿using System.Text;
using Steamworks;

namespace Facepunch.Steamworks.Abstractions.Wrappers;

public sealed class SteamRemoteStorageWrapper : ISteamRemoteStorage
{
    public byte[] FileRead(string fileName)
    {
        return SteamRemoteStorage.FileRead(fileName);
    }

    public string FileReadText(string fileName)
    {
        var bytes = FileRead(fileName);
        return bytes == null ? string.Empty : Encoding.ASCII.GetString(bytes);
    }

    public bool FileWrite(string fileName, byte[] data)
    {
        return SteamRemoteStorage.FileWrite(fileName, data);
    }

    public bool FileWrite(string fileName, string fileText)
    {
        var bytes = Encoding.ASCII.GetBytes(fileText);
        return FileWrite(fileName, bytes);
    }

    public bool FileExists(string fileName)
    {
        return SteamRemoteStorage.FileExists(fileName);
    }
}