﻿using System.Collections.Generic;

namespace Facepunch.Steamworks.Abstractions.Wrappers;

public class ResultsPage : IResultPage
{
    public int ResultCount { get; }
    public IEnumerable<IItem> Entries { get; }

    public ResultsPage(int resultCount, IEnumerable<IItem> entries)
    {
        ResultCount = resultCount;
        Entries = entries;
    }
}