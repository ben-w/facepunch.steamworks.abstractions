﻿using System;
using System.Threading.Tasks;
using Steamworks;
using Steamworks.Ugc;

namespace Facepunch.Steamworks.Abstractions.Wrappers;

public class EditorWrapper : IEditor
{
    private Editor _editor;
    
    public EditorWrapper(WorkshopFileType fileType)
    {
        switch (fileType)
        {
            case WorkshopFileType.Community:
                _editor = Editor.NewCommunityFile;
                break;
            case WorkshopFileType.Collection:
                _editor = Editor.NewCollection;
                break;
            case WorkshopFileType.Microtransaction:
                _editor = Editor.NewMicrotransactionFile;
                break;
            case WorkshopFileType.GameManagedItem:
                _editor = Editor.NewGameManagedFile;
                break;
            default:
                throw new NotImplementedException();
        }
    }

    public EditorWrapper(IPublishedFieldId publishedFieldId)
    {
        _editor = new Editor(publishedFieldId.Value);
    }

    public IEditor ForAppId(AppId id)
    {
        _editor.ForAppId(id);
        return this;
    }

    public IEditor WithTitle(string title)
    {
        _editor.WithTitle(title);
        return this;
    }

    public IEditor WithDescription(string description)
    {
        _editor.WithDescription(description);
        return this;
    }

    public IEditor WithMetaData(string metaData)
    {
        _editor.WithMetaData(metaData);
        return this;
    }

    public IEditor WithChangeLog(string changeLog)
    {
        _editor.WithChangeLog(changeLog);
        return this;
    }

    public IEditor InLanguage(string language)
    {
        _editor.InLanguage(language);
        return this;
    }

    public IEditor WithPreviewFile(string previewFilePath)
    {
        _editor.WithPreviewFile(previewFilePath);
        return this;
    }

    public IEditor WithContent(string folderName)
    {
        _editor.WithContent(folderName);
        return this;
    }

    public IEditor WithPublicVisibility()
    {
        _editor.WithPublicVisibility();
        return this;
    }

    public IEditor WithFriendsOnlyVisibility()
    {
        _editor.WithFriendsOnlyVisibility();
        return this;
    }

    public IEditor WithPrivateVisibility()
    {
        _editor.WithPrivateVisibility();
        return this;
    }

    public IEditor WithUnlistedVisibility()
    {
        _editor.WithUnlistedVisibility();
        return this;
    }

    public IEditor WithTag(string tag)
    {
        _editor.WithTag(tag);
        return this;
    }

    public Task<PublishResult> SubmitAsync(IProgress<float> progress = null, Action<PublishResult> onItemCreated = null)
    {
        return _editor.SubmitAsync(progress, onItemCreated);
    }
}