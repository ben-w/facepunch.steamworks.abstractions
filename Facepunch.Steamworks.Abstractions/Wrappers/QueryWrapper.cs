﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Steamworks;
using Steamworks.Ugc;

namespace Facepunch.Steamworks.Abstractions.Wrappers;

public class QueryWrapper : IQuery
{
    private Query _query;

    public QueryWrapper()
    {
        _query = new Query();
    }

    public QueryWrapper(UgcType type)
    {
        _query = new Query(type);
    }
    public IQuery WhereUserSubscribed()
    {
        _query = _query.WhereUserSubscribed();
        return this;
    }

    public IQuery WithLongDescription(bool b)
    {
        _query = _query.WithLongDescription(b);
        return this;
    }

    public async Task<IResultPage?> GetPageAsync(int page)
    {
        var result = await _query.GetPageAsync(page);
        
        if (!result.HasValue)
        {
            return null;
        }

        List<ItemWrapper> entries = new (result.Value.ResultCount);
        foreach (var item in result.Value.Entries)
        {
            SteamFriends.RequestUserInformation(item.Owner.Id);
            entries.Add(new ItemWrapper(item));
        }

        return new ResultsPage(result.Value.ResultCount, entries);
    }
    
    public IQuery WithFileId(ulong id)
    {
        _query = _query.WithFileId(id);
        return this;
    }

    public IQuery WhereUserPublished(SteamId user = default (SteamId))
    {
        _query = _query.WhereUserPublished();
        return this;
    }

    public IQuery WithDifferentApp(AppId consumerAppId, AppId creatorAppId)
    {
        _query = _query.WithDifferentApp(consumerAppId, creatorAppId);
        return this;
    }

    public IQuery WithMetadata(bool value)
    {
        _query = _query.WithMetadata(value);
        return this;
    }
}