﻿using Steamworks.Data;

namespace Facepunch.Steamworks.Abstractions.Wrappers;

public class PublishedFieldIdWrapper : IPublishedFieldId
{
    public ulong Value { get; }

    public PublishedFieldIdWrapper(PublishedFileId publishedFileId)
    {
        Value = publishedFileId.Value;
    }
}