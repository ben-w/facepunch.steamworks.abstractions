﻿using System.Threading.Tasks;
using Steamworks;

namespace Facepunch.Steamworks.Abstractions;

public interface IQuery
{
    IQuery WhereUserSubscribed();
    
    IQuery WithLongDescription(bool b);

    Task<IResultPage?> GetPageAsync(int page);
    
    IQuery WithFileId(ulong id);

    IQuery WhereUserPublished(SteamId user = default(SteamId));

    IQuery WithDifferentApp(AppId consumerAppId, AppId creatorAppId);
    
    IQuery WithMetadata(bool value);
}