﻿using System;
using System.Threading.Tasks;
using Steamworks;
using Steamworks.Ugc;

namespace Facepunch.Steamworks.Abstractions;

public interface IEditor
{
    IEditor ForAppId(AppId id);
    IEditor WithTitle(string title);
    IEditor WithDescription(string description);
    IEditor WithMetaData(string metaData);
    IEditor WithChangeLog(string changeLog);
    IEditor InLanguage(string language);
    IEditor WithPreviewFile(string previewFilePath);
    IEditor WithContent(string folderName);
    IEditor WithPublicVisibility();
    IEditor WithFriendsOnlyVisibility();
    IEditor WithPrivateVisibility();
    IEditor WithUnlistedVisibility();
    IEditor WithTag(string tag);
    Task<PublishResult> SubmitAsync(IProgress<float> progress = null, Action<PublishResult> onItemCreated = null);
}