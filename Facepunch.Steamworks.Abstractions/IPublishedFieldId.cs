﻿namespace Facepunch.Steamworks.Abstractions;

public interface IPublishedFieldId
{
    public ulong Value { get; }
}