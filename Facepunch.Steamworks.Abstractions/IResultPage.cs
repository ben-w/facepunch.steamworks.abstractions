﻿using System.Collections.Generic;

namespace Facepunch.Steamworks.Abstractions;

public interface IResultPage
{
    public int ResultCount { get; }
    public IEnumerable<IItem> Entries { get; }
}