﻿namespace Facepunch.Steamworks.Abstractions;

public interface ISteamRemoteStorage
{
    byte[] FileRead(string fileName);
    string FileReadText(string fileName);
    bool FileWrite(string fileName, byte[] data);
    bool FileWrite(string fileName, string fileText);
    bool FileExists(string fileName);
}