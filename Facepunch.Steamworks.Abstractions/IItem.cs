﻿using System;
using System.Threading.Tasks;

namespace Facepunch.Steamworks.Abstractions;

public interface IItem
{
    IPublishedFieldId Id { get; }
    string Title { get; }
    string Description { get; }
    bool IsSubscribed { get; }
    string[] Tags { get; }
    string PreviewImageUrl { get; }
    uint VotesDown { get; }
    uint VotesUp { get; }
    ulong NumSubscriptions { get; }
    string OwnerName { get; }
    DateTime Updated { get; }
    string MetaData { get; }
    bool IsPublic { get; }
    bool IsFriendsOnly { get; }
    bool IsPrivate { get; }
    bool IsUnlisted { get; }
    

    Task<bool> Subscribe();

    Task<bool> Unsubscribe();
}