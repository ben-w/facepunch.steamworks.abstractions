﻿namespace Facepunch.Steamworks.Abstractions;

public interface IEditorFactory
{
    IEditor New(WorkshopFileType type);

    IEditor New(IPublishedFieldId publishedFieldId);
}