# Facepunch.Steamworks.Abstractions

Just like [System.IO.Abstractions](https://github.com/TestableIO/System.IO.Abstractions), but for Facepunch.Steamworks. Yay for testable Steamworks! 

## Project status

This project was created for the want to test some code with Steam works in a hobby project. So this being a part of that project, development will be very slow/only for areas I need.

If you have an area you want, consider forking this and sending a merge request back so others can use your amazing work! 